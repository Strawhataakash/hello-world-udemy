#!/bin/bash
set -e

scriptloc=$(dirname "$(realpath "$0")")
cd "$scriptloc"

echo "Image Build start..."
sudo rm -rf workdir
sudo mkdir workdir
cd workdir

echo "Cloning start:" $1
repo_url="https://gitlab.com/Strawhataakash/hello-world-udemy.git"
sudo git clone --depth 1 "$repo_url"

cd hello-world-udemy
sudo mvn clean package -Djava.net.preferIPv4Addresses=true -X

cd ../..
echo "Building Docker Image using docker file"
sudo docker build -t testcat .

echo "Create Tag to push image to dockerhub"
sudo docker tag testcat strawhataakash/testcat:$1

echo "Pushing docker image to dockerhub"
sudo docker push strawhataakash/testcat:$1

echo "Deployment done.."

