variable "ssh_private_key_path" {
  description = "Path to SSH private key"
  type        = string
  default     = "/home/ubuntu/.ssh/id_rsa"  # Default path to local SSH private key
}

variable "jenkins_tag_id" {
  description = "Tag_ID value from Jenkins"
}