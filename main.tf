provider "null" {
}

#SSH connection block
resource "null_resource" "execute_script" {
  # Use a dummy trigger to force the provisioner to run on each apply
    triggers = {
      always_run = timestamp()
    }

  connection {
    type = "ssh"
    user = "ubuntu"
    host = "15.206.93.30"
    private_key = file("${var.ssh_private_key_path}")
  }

#use remote exec comamnd to execute shell script commands 
provisioner "remote-exec" {
  inline = [ 
    "sudo chmod 755 /opt/docker/deploy.sh",
    "sudo /opt/docker/deploy.sh ${var.jenkins_tag_id}"
      ]
}
}